package ru.korkmasov.tsc.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.korkmasov.tsc.api.service.ServiceLocator;
import ru.korkmasov.tsc.enumerated.Role;
import ru.korkmasov.tsc.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public final class AdminUserEndpoint extends AbstractEndpoint implements ru.korkmasov.tsc.api.endpoint.IAdminUserEndpoint {

    public AdminUserEndpoint() {
        super(null);
    }

    public AdminUserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void lockUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "login", partName = "login") @NotNull final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().lockUserByLogin(login);
    }

    @Override
    @WebMethod
    public void removeUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "login", partName = "login") @NotNull final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().removeByLogin(login);
    }

    @Override
    @WebMethod
    public void unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "login", partName = "login") @NotNull final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().unlockUserByLogin(login);
    }

}
