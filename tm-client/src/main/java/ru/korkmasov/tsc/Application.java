package ru.korkmasov.tsc;

import ru.korkmasov.tsc.bootstrap.Bootstrap;
import ru.korkmasov.tsc.util.SystemUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class Application {

    public static void main(@Nullable String[] args) {
        System.out.println(SystemUtil.getPID());
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }
}