package ru.korkmasov.tsc.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.api.repository.ISessionRepository;
import ru.korkmasov.tsc.api.service.IPropertyService;
import ru.korkmasov.tsc.api.service.ISessionService;
import ru.korkmasov.tsc.api.service.ServiceLocator;
import ru.korkmasov.tsc.enumerated.Role;
import ru.korkmasov.tsc.exception.system.AccessDeniedException;
import ru.korkmasov.tsc.model.Session;
import ru.korkmasov.tsc.model.User;
import ru.korkmasov.tsc.util.HashUtil;
import ru.korkmasov.tsc.util.SignatureUtil;

import java.util.List;

import static ru.korkmasov.tsc.util.ValidationUtil.isEmpty;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final ISessionRepository sessionRepository;
    @NotNull
    private ServiceLocator serviceLocator;


    public SessionService(
            @NotNull final ISessionRepository sessionRepository,
            @NotNull final ServiceLocator serviceLocator
    ) {
        super(sessionRepository);
        this.sessionRepository = sessionRepository;
        this.serviceLocator = serviceLocator;
    }

    @Override
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (isEmpty(login) || isEmpty(password)) return false;
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return false;
        @Nullable final String passwordHash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        if (isEmpty(passwordHash)) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    public void close(@NotNull final Session session) {
        validate(session);
        remove(session);
    }

    @Override
    public void closeAll(@NotNull final Session session) {
        validate(session);
        sessionRepository.removeByUserId(session.getUserId());
    }

    @Nullable
    @Override
    public List<Session> getListSession(@NotNull final Session session) {
        validate(session);
        return sessionRepository.findByUserId(session.getUserId());
    }

    @Nullable
    @Override
    public User getUser(@NotNull final Session session) {
        @NotNull final String userId = getUserId(session);
        return serviceLocator.getUserService().findById(userId);
    }

    @NotNull
    @Override
    public String getUserId(@NotNull final Session session) {
        validate(session);
        return session.getUserId();
    }

    @Override
    public boolean isValid(@NotNull final Session session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Nullable
    @Override
    public Session open(@Nullable final String login, @Nullable final String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return null;
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.add(session);
        return sign(session);
    }

    @Nullable
    @Override
    public Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @Nullable final String salt = propertyService.getSessionSalt();
        final int cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

    @Override
    public void signOutByLogin(@Nullable final String login) {
        if (isEmpty(login)) return;
    }

    @Override
    public void signOutByUserId(@Nullable String userId) {
        return;
    }

    @Override
    public void validate(@NotNull final Session session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (user.getRole() == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    public void validate(@Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        if (isEmpty(session.getSignature())) throw new AccessDeniedException();
        if (isEmpty(session.getUserId())) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final Session sessionSign = sign(temp);
        if (sessionSign == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sessionSign.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        if (!sessionRepository.contains(session.getId())) throw new AccessDeniedException();
    }
}
