package ru.korkmasov.tsc.repository;

import ru.korkmasov.tsc.api.repository.IOwnerRepository;
import ru.korkmasov.tsc.model.AbstractOwner;
import ru.korkmasov.tsc.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractOwnerRepository<E extends AbstractOwner> extends AbstractRepository<E> implements IOwnerRepository<E> {

    @NotNull
    protected final List<E> list = new ArrayList<>();

    @Nullable
    public List<E> findAll(@NotNull final String userId) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public E findById(@NotNull final String userId, @NotNull final String id) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .findFirst().orElse(null);
    }

    @Override
    public int size(@NotNull String userId) {
        int i = 0;
        for (final E e : list) {
            if (userId.equals(e.getUserId())) i++;
        }
        return i;
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        for (final E e : list) {
            if (id.equals(e.getId()) && userId.equals(e.getUserId())) return true;
        }
        return false;
    }

    @Override
    public void clear(@NotNull final String userId) {
        list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .forEach(list::remove);
    }

    @Nullable
    @Override
    public Project removeById(@NotNull final String userId, @NotNull final String id) {
        list.stream()
                .filter(e -> userId.equals(e.getUserId()) && id.equals(e.getId()))
                .findFirst()
                .ifPresent(this::remove);
        return null;
    }

}