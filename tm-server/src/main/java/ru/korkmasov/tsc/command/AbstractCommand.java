package ru.korkmasov.tsc.command;

import ru.korkmasov.tsc.enumerated.Role;
import ru.korkmasov.tsc.api.service.ServiceLocator;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String arg();

    public abstract String name();

    public abstract String description();

    public abstract void execute();

    public Role[] roles() { return null; }

}

