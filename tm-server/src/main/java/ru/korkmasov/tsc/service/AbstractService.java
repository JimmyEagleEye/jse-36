package ru.korkmasov.tsc.service;

import ru.korkmasov.tsc.api.repository.IRepository;
import ru.korkmasov.tsc.api.service.IService;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.model.AbstractEntity;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IRepository<E> repository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public void addAll(@Nullable final Collection<E> collection) {
        @NotNull final Optional<Collection<E>> optional = Optional.ofNullable(collection);
        optional.ifPresent(repository::addAll);
    }

    @Override
    public int size(@NotNull String testUserId) {
        return repository.size(TEST_USER_ID);
    }

    @Override
    public void add(@NotNull final E entity) {
        repository.add(entity);
    }

    @Override
    public void remove(@Nullable final E entity) {
        if (entity == null) return;
        repository.remove(entity);
    }

    @Nullable
    public E findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Nullable
    public E removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        repository.removeById(id);
        return null;
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public void clear() {
        repository.clear();
    }

}
